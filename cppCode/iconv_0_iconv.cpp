#include "..\include_iconv_header.h"

// 调用格式: SDT_BIN 编码转换, 命令说明: "将指定的数据从一种编码转换为另一种编码，返回转换后的数据。如果执行失败，将返回空字节集，同时设置参数“执行结果”为“假”。本命令相当于“编码转换_打开()”“编码转换_转换()”“编码转换_关闭()”三个命令的组合使用。"
// 参数<1>: 被转换数据 SDT_BIN, 参数说明: NULL
// 参数<2>: 转换前的数据编码 SDT_TEXT, 参数说明: "可以使用本库中定义的以“编码_”开头的编码常量。编码名称不区分字母大小写。注意：如果指定的编码与“被转换数据”的实际编码不符，很可能导致编码转换失败。"
// 参数<3>: 转换后的数据编码 SDT_TEXT, 参数说明: "可以使用本库中定义的以“编码_”开头的编码常量。编码名称不区分字母大小写。"
// 参数<4>: [&执行结果 SDT_BOOL], 参数说明: "如果提供本参数，其中将被写入本命令的执行结果——执行成功时为真，执行失败时为假。"
ICONV_EXTERN_C void Kiiconv_kiiconv_0_Kiiconv(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

	//获取字节集长度和数据指针
	size_t dwDataLen = 0;//待转换数据长度
	LPBYTE pData = 0;//待转换的数据
	DATA_TYPE dtDataType = pArgInf[0].m_dtDataType & ~DT_IS_VAR;//数据类型
	bool dtIsVar = (pArgInf[0].m_dtDataType & DT_IS_VAR) != 0;//是否为变量地址

	switch (dtDataType)
	{
	case SDT_BIN:
		//字节集数据
		if (dtIsVar) {
			pData = GetAryElementInf(*pArgInf[0].m_ppBin, (LPINT)&dwDataLen);//待转换数据指针
		}
		else {
			pData = GetAryElementInf(pArgInf[0].m_pBin, (LPINT)&dwDataLen);//待转换数据指针
		}

		if (dwDataLen == 0) {//数据长度为0，直接返回
			if (pArgInf[3].m_pBool) *pArgInf[3].m_pBool = false;
			return;
		}
		break;
	case SDT_TEXT:
		//文本数据
		if (dtIsVar) {
			pData = (LPBYTE)*pArgInf[0].m_ppText;
			dwDataLen = strlen(*pArgInf[0].m_ppText);
		}
		else {
			pData = (LPBYTE)pArgInf[0].m_pText;
			dwDataLen = strlen(pArgInf[0].m_pText);
		}
		break;
	default:
		if (pArgInf[3].m_pBool) *pArgInf[3].m_pBool = false;
		return;
	}

	iconv_t cd = iconv_open(pArgInf[2].m_pText, pArgInf[1].m_pText);
	if (cd == (iconv_t)-1) {
		if (pArgInf[3].m_pBool) *pArgInf[3].m_pBool = false;
		return;
	}

	size_t dwBuffLen = dwDataLen * 4;//缓冲区长度
	LPBYTE pBuff = (LPBYTE)ealloc(sizeof(INT) * 2 + dwBuffLen);
	char** outbuf = (char**)(pBuff + sizeof(INT) * 2);
	size_t outbytesleft = dwBuffLen;

	//后面4个参数都会改变，当执行一次后，pData 会移动到 pData+dwDataLen 的位置，dwDataLen=0 ，pBuff 也会移动到新的位置，dwBuffLen 返回 pBuff 还剩下多少
	size_t ret = iconv(cd, (char**)&pData, &dwDataLen, (char**)&outbuf, &outbytesleft);
	if (ret == -1) {
		if (pArgInf[3].m_pBool) *pArgInf[3].m_pBool = false;
		efree(pBuff);
		iconv_close(cd);
		return;
	}
	*(LPINT)pBuff = 1;
	*(LPINT)(pBuff + sizeof(INT)) = dwBuffLen - outbytesleft;
	pRetData->m_pBin = pBuff;
	if (pArgInf[3].m_pBool) *pArgInf[3].m_pBool = true;
	iconv_close(cd);
}



// 调用格式: _SDT_ALL 编码转换, 命令说明: "将指定的数据从一种编码转换为另一种编码，返回转换后的数据。如果执行失败，将返回空字节集，同时设置参数“执行结果”为“假”。本命令相当于“编码转换_打开()”“编码转换_转换()”“编码转换_关闭()”三个命令的组合使用。"
// 参数<1>: 被转换数据 _SDT_ALL, 参数说明: NULL
// 参数<2>: 转换前的数据编码 SDT_TEXT, 参数说明: "可以使用本库中定义的以“编码_”开头的编码常量。编码名称不区分字母大小写。注意：如果指定的编码与“被转换数据”的实际编码不符，很可能导致编码转换失败。"
// 参数<3>: 转换后的数据编码 SDT_TEXT, 参数说明: "可以使用本库中定义的以“编码_”开头的编码常量。编码名称不区分字母大小写。"
// 参数<4>: [返回文本型 SDT_BOOL], 参数说明: "结果返回文本型，默认返回字节集型。"
// 参数<5>: [&执行结果 SDT_BOOL], 参数说明: "如果提供本参数，其中将被写入本命令的执行结果——执行成功时为真，执行失败时为假。"
ICONV_EXTERN_C void Kiiconv_kiiconvEx_4_Kiiconv(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

	//获取字节集长度和数据指针
	size_t dwDataLen = 0;//待转换数据长度
	LPBYTE pData = 0;//待转换的数据
	DATA_TYPE dtDataType = pArgInf[0].m_dtDataType & ~DT_IS_VAR;//数据类型
	bool dtIsVar = (pArgInf[0].m_dtDataType & DT_IS_VAR) != 0;//是否为变量地址

	switch (dtDataType)
	{
	case SDT_BIN:
		//字节集数据
		if (dtIsVar) {
			pData = GetAryElementInf(*pArgInf[0].m_ppBin, (LPINT)&dwDataLen);//待转换数据指针
		}
		else {
			pData = GetAryElementInf(pArgInf[0].m_pBin, (LPINT)&dwDataLen);//待转换数据指针
		}

		if (dwDataLen == 0) {//数据长度为0，直接返回
			if (pArgInf[4].m_pBool) *pArgInf[4].m_pBool = false;
			return;
		}
		break;
	case SDT_TEXT:
		//文本数据
		if (dtIsVar) {
			pData = (LPBYTE)*pArgInf[0].m_ppText;
			dwDataLen = strlen(*pArgInf[0].m_ppText);
		}
		else {
			pData = (LPBYTE)pArgInf[0].m_pText;
			dwDataLen = strlen(pArgInf[0].m_pText);
		}
		break;
	default:
		*pArgInf[4].m_pBool = false;
		return;
	}

	iconv_t cd = iconv_open(pArgInf[2].m_pText, pArgInf[1].m_pText);
	if (cd == (iconv_t)-1) {
		if (pArgInf[4].m_pBool) *pArgInf[4].m_pBool = false;
		return;
	}

	size_t dwBuffLen = dwDataLen * 4;//缓冲区长度
	LPBYTE pBuff;
	char** outbuf;
	if (pArgInf[3].m_pBool) {//返回文本型
		pBuff = (LPBYTE)ealloc(dwBuffLen);
		outbuf = (char**)(pBuff);
	}
	else {
		pBuff = (LPBYTE)ealloc(sizeof(INT) * 2 + dwBuffLen);
		outbuf = (char**)(pBuff + sizeof(INT) * 2);
	}

	size_t outbytesleft = dwBuffLen;

	//后面4个参数都会改变，当执行一次后，pData 会移动到 pData+dwDataLen 的位置，dwDataLen=0 ，pBuff 也会移动到新的位置，dwBuffLen 返回 pBuff 还剩下多少
	size_t ret = iconv(cd, (char**)&pData, &dwDataLen, (char**)&outbuf, &outbytesleft);
	if (ret == -1) {
		if (pArgInf[4].m_pBool) *pArgInf[4].m_pBool = false;
		efree(pBuff);
		iconv_close(cd);
		return;
	}
	if (!pArgInf[3].m_pBool) {
		*(LPINT)pBuff = 1;
		*(LPINT)(pBuff + sizeof(INT)) = dwBuffLen - outbytesleft;
	}
	pRetData->m_dtDataType = pArgInf[3].m_pBool ? SDT_TEXT : SDT_BIN;
	pRetData->m_pBin = pBuff;
	if(pArgInf[4].m_pBool) *pArgInf[4].m_pBool = true;
	iconv_close(cd);
}




// 调用格式: SDT_BOOL 文件编码转换, 命令说明: "将指定的数据从一种编码转换为另一种编码，返回转换后的数据。如果执行失败，将返回假，本命令相当于“编码转换_打开()”“编码转换_转换()”“编码转换_关闭()”三个命令的组合使用。"
// 参数<1>: 转换前的数据编码 SDT_TEXT, 参数说明: "可以使用本库中定义的以“编码_”开头的编码常量。编码名称不区分字母大小写。注意：如果指定的编码与“被转换数据”的实际编码不符，很可能导致编码转换失败。"
// 参数<2>: 转换后的数据编码 SDT_TEXT, 参数说明: "可以使用本库中定义的以“编码_”开头的编码常量。编码名称不区分字母大小写。"
// 参数<3>: 转换前的文件 SDT_TEXT, 参数说明: "转换前的文件，请填写文件路径，不能和转换后的文件是同一文件。"
// 参数<4>: 转换后的文件 SDT_TEXT, 参数说明: "转换后的文件，请填写文件路径，不能和转换前的文件是同一文件。"
ICONV_EXTERN_C void Kiiconv_kiiconv_file_6_Kiiconv(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	//先打开两个文件
	HANDLE hRead = CreateFile(pArgInf[2].m_pText, GENERIC_READ, FILE_SHARE_READ| FILE_SHARE_WRITE,NULL,OPEN_EXISTING,NULL,NULL);
	if (hRead == INVALID_HANDLE_VALUE) {
		pRetData->m_bool = false; 
		return;
	}
		
	HANDLE hWrite = CreateFile(pArgInf[3].m_pText, GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, NULL, NULL);
	if (hWrite == INVALID_HANDLE_VALUE) {
		CloseHandle(hRead);
		pRetData->m_bool = false; 
		return;
	}
	
	//打开转换流
	iconv_t cd = iconv_open(pArgInf[1].m_pText, pArgInf[0].m_pText);
	if (cd != (iconv_t)-1) {
		//读入文件缓冲区 1M
		
		DWORD nNumberOfBytesToWrite = 1048576, lpNumberOfBytesWritten = 0;
		LPBYTE lpInBuffer=(LPBYTE)malloc(nNumberOfBytesToWrite);
		LPVOID pInData;
		
		//转换缓冲区 4M(因为不知道转换后的长度，按照1字节转换为4字节计算的缓冲区，当然可以自己修改)
		DWORD dwOutLen = 4194304;
		size_t ret, outbytesleft=0,dwDataLen=0;
		LPBYTE lpOutBuffer = (LPBYTE)malloc(dwOutLen);
		LPVOID pOutData;

		//循环读出文件数据，经过iconv 转换后再写出数据
		while (ReadFile(hRead, lpInBuffer, nNumberOfBytesToWrite, &lpNumberOfBytesWritten, NULL) && lpNumberOfBytesWritten)
		{
			//重新赋值
			pInData = lpInBuffer;
			pOutData = lpOutBuffer;
			outbytesleft = dwOutLen;

			//转换编码
			ret=iconv(cd, (char**)&pInData, (size_t*)&lpNumberOfBytesWritten, (char**)&pOutData, &outbytesleft);
			if (ret == -1) {
				break;//失败跳出
			}

			//如果数据没有处理完成，方便处理，文件读写位置往前移动
			if (lpNumberOfBytesWritten) {
				SetFilePointer(hRead, -(int)lpNumberOfBytesWritten, 0, FILE_CURRENT);
			}

			dwDataLen = dwOutLen - outbytesleft;//计算转换后的数据长度
			//写出转换后的数据
			if (!WriteFile(hWrite, lpOutBuffer, dwDataLen, &lpNumberOfBytesWritten, NULL)) {
				ret = 99;
				break;//失败跳出
			}
		}
		pRetData->m_bool = ret ==0;
		free(lpOutBuffer);
		free(lpInBuffer);
		iconv_close(cd);
	}
	
	//释放文件
	CloseHandle(hWrite);
	CloseHandle(hRead);
}
